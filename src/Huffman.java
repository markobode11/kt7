import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {
    // lahenduse idee voetud aadressilt https://www.geeksforgeeks.org/huffman-coding-greedy-algo-3/

    /**
     * Node class to create Huffman tree.
     */
    static class HuffmanNode {
        /**
         * Node`s frequency
         */
        int freq;

        /**
         * Byte that is in this Node.
         */
        Byte b;

        /**
         * Left child
         */
        HuffmanNode left;

        /**
         * Right child
         */
        HuffmanNode right;
    }

    /**
     * Comparator to compare two Nodes by their frequency`s.
     */
    static class MyComparator implements Comparator<HuffmanNode> {
        public int compare(HuffmanNode x, HuffmanNode y) {
            return x.freq - y.freq;
        }
    }

    /**
     * Initially added text.
     */
    private final byte[] text;

    /**
     * Array of all the letter`s in original text as bytes.
     */
    private Byte[] byteArray;

    /**
     * Array of each letter`s frequency.
     */
    private Integer[] freqArray;

    /**
     * Maps bytes to their hCodes.
     */
    private final HashMap<Byte, String> hCodeMap = new HashMap<>();

    /**
     * Constructor to build the Huffman code for a given bytearray.
     *
     * @param text source data
     */
    Huffman(byte[] text) {
        if (text == null) {
            throw new RuntimeException("Cannot create Huffman class with empty text!");
        }
        this.text = text;
    }

    /**
     * Length of encoded data in bits.
     *
     * @return number of bits
     */
    public int bitLength() {
        int length = 0;
        for (byte b : text) {
            length += hCodeMap.get(b).length();
        }
        return length;
    }


    /**
     * Encoding the byte array using this prefixcode.
     *
     * @param origData original data
     * @return encoded data
     */
    public byte[] encode(byte[] origData) {

        HashMap<Byte, Integer> freqMap = getFreqMap(origData);

        byteArray = freqMap.keySet().toArray(new Byte[0]);
        freqArray = freqMap.values().toArray(new Integer[0]);

        PriorityQueue<HuffmanNode> q = new PriorityQueue<>(byteArray.length, new MyComparator());

        for (int i = 0; i < byteArray.length; i++) {
            HuffmanNode node = createNode(i);
            q.add(node);
        }

        HuffmanNode root = createRootNode(q);
        getHCodes(root, "");

        byte[] encoded = new byte[(int) Math.ceil(bitLength() / 8.0)];
        StringBuilder currentByte = new StringBuilder();
        int pos = 0;
        for (byte b : origData) {
            currentByte.append(hCodeMap.get(b));
            if (currentByte.length() > 8) {
                String tmp = currentByte.substring(0, 8);
                int intValue = Integer.parseInt(tmp, 2);
                encoded[pos] = (byte) intValue;
                pos++;
                currentByte = new StringBuilder(currentByte.substring(8));
            }
        }
        int intValue = Integer.parseInt(currentByte.toString(), 2);
        encoded[pos] = (byte) intValue;
        return encoded;
    }

    /**
     * Decoding the byte array using this prefixcode.
     *
     * @param encodedData encoded data
     * @return decoded data (hopefully identical to original)
     */
    public byte[] decode(byte[] encodedData) {
        byte[] decoded = new byte[text.length];

        String binaryString = getBinaryString(encodedData);

        StringBuilder currentHCode = new StringBuilder();
        int pos = 0;
        for (int i = 0; i < binaryString.length(); i++) {
            currentHCode.append(binaryString.charAt(i));
            if (hCodeMap.containsValue(currentHCode.toString())) {
                for (byte b : hCodeMap.keySet()) {
                    if (hCodeMap.get(b).equals(currentHCode.toString())) {
                        decoded[pos] = b;
                        pos++;
                        currentHCode = new StringBuilder();
                        break;
                    }
                }
            }
        }
        return decoded;
    }

    /**
     * Create Huffman node of set position in the byteArray.
     *
     * @param pos position in the byte array
     * @return created Node
     */
    private HuffmanNode createNode(int pos) {

        HuffmanNode node = new HuffmanNode();

        node.b = byteArray[pos];
        node.freq = freqArray[pos];

        node.left = null;
        node.right = null;
        return node;
    }

    /**
     * Create root node from the Queue.
     * @param q priority queue. As the node`s frequency gets lower it`s priority gets higher
     * @return root node of the Huffman tree
     */
    private HuffmanNode createRootNode(PriorityQueue<HuffmanNode> q) {
        HuffmanNode root = new HuffmanNode();
        root.left = q.peek();
        root.right = null;
        root.b = null;

        while (q.size() > 1) {
            HuffmanNode x = q.poll();
            HuffmanNode y = q.poll();

            HuffmanNode f = new HuffmanNode();

            f.freq = x.freq + y.freq;
            f.b = null;

            f.left = x;
            f.right = y;

            root = f;
            q.add(f);
        }
        return root;
    }

    /**
     * Count all bytes and map each byte to their frequency
     * @param text count bytes from this
     * @return frequency map
     */
    private HashMap<Byte, Integer> getFreqMap(byte[] text) {
        HashMap<Byte, Integer> freqMap = new HashMap<>();
        for (byte b : text) {
            if (freqMap.containsKey(b)) {
                int oldValue = freqMap.get(b);
                freqMap.replace(b, oldValue, oldValue + 1);
            } else {
                freqMap.put(b, 1);
            }
        }
        return freqMap;
    }

    /**
     * Calculate each byte`s hCode from Huffman`s tree root node
     * @param root Huffman`s tree root node
     * @param s shows if hCode should add 0 or 1
     */
    public void getHCodes(HuffmanNode root, String s) {
        if (root.left == null && root.right == null && root.b != null) {
            hCodeMap.putIfAbsent(root.b, s);
        } else if (root.left != null && root.b == null) {
            getHCodes(root.left, s + "0");
            if (root.right != null) {
                getHCodes(root.right, s + "1");
            }
        }
    }

    /**
     * Add all bytes together as a string.
     * @param encodedData data encoded by this Huffman class object
     * @return all bytes combined as a string
     */
    private String getBinaryString(byte[] encodedData) {
        StringBuilder data = new StringBuilder();
        int count = 0;
        int totalBits = bitLength();
        for (byte b : encodedData) {
            String binary = String.format("%8s", Integer.toBinaryString(b)).replaceAll(" ", "0");

            if (binary.length() > 8) {
                binary = binary.substring(binary.length() - 8);
            }


            if (count + 8 > totalBits) {
                binary = binary.substring(8 - (totalBits - count));
            }

            data.append(binary);
            count += binary.length();
        }
        return data.toString();
    }

    /**
     * Main method.
     */
    public static void main(String[] params) {
        String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
        byte[] orig = tekst.getBytes();
        System.out.println(Arrays.toString(orig));
        Huffman huf = new Huffman(orig);
        byte[] kood = huf.encode(orig);
        byte[] orig2 = huf.decode(kood);
        System.out.println(Arrays.toString(orig2));
        // must be equal: orig, orig2
        System.out.println(Arrays.equals(orig, orig2));
        int lngth = huf.bitLength();
        System.out.println("Length of encoded data in bits: " + lngth);
    }
}
